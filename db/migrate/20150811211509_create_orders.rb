class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :room_id
      t.integer :quantity
      t.integer :reservation_id

      t.timestamps null: false
    end
  end
end
