class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|

    	t.string :name
			t.string :ci
			t.string :company
			t.string :date
			t.string :email
			t.string :telephone
			t.string :event_type
			t.string :quantity
			t.string :begin_date
			t.string :end_date
			t.string :begin_time
			t.string :end_time
			t.string :assembly_type
			t.text :event_description
			t.boolean :cold_snacks
			t.boolean :hot_snacks
			t.boolean :dish_menu
			t.boolean :buffet_type
			t.boolean :theme_stations
			t.boolean :morning_coffeebreak
			t.boolean :evening_coffeebreak
			t.boolean :cheese_table
			t.boolean :uncorking_coctels
			t.boolean :uncorking_whisky
			t.boolean :uncorking_wine
			t.boolean :uncorking_vodka
			t.boolean :coctel_bar
			t.boolean :sodas
			t.string :color_theme
			t.boolean :flower_arrengements
			t.boolean :baloons
			t.boolean :toldo
			t.boolean :sound
			t.boolean :dj
			t.boolean :musical_group
			t.boolean :podium
			t.boolean :videobeam
			t.boolean :lights
			t.boolean :inflatable
			t.boolean :magic_show
			t.boolean :dance_show
			t.boolean :crazy_hour
			t.text :other_services
			t.text :observations

      t.timestamps null: false
    end
  end
end
