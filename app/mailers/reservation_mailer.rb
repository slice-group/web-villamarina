class ReservationMailer < ApplicationMailer
  default from: "from@example.com"

	def reservation_email(params, link)
		@params = params
		@link = link
		mail(to: 'info@posadavillamarina.com', subject: 'Nueva solicitud de Reservación.')
	end

end
