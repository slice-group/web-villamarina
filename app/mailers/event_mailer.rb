class EventMailer < ApplicationMailer
  default from: "from@example.com"

	def event_email(evento)
		@evento = evento
		mail(to: 'info@posadavillamarina.com', subject: @evento.name+" ha enviado una solicitud de Evento.")
	end

end