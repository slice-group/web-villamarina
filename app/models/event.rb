class Event < ActiveRecord::Base

  validates_presence_of :name, :ci, :email, :telephone, :begin_date, :end_date, :assembly_type
  #validacion de cedula
  validates :ci, :length => { :minimum => 7, :maximum => 8 }
  validates_numericality_of :ci, :only_integer => true

  #validaciones para telefono
  validates :telephone, :length => { :minimum => 11, :maximum => 13 }
  validates_numericality_of :telephone, :only_integer => true

  #validacion de email
  validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create }

  #validacion de cantidad de personas
  validates :quantity, :numericality => { :less_than_or_equal_to => 100 }

  #custom validate
  validate :end_date_greater_than_begin_date?

  def chosen(selection)
    if selection == true
      return "Si"
    else
      return "No"
    end
  end

  def blank(text)
    if text.blank?
      return "N/A"
    end
  end

  def end_date_greater_than_begin_date?
    unless self.begin_date.blank? or self.end_date.blank?
      errors.add(:end_date, "El check-out no puede ser despues del check-in") if self.begin_date > self.end_date
    end
  end

end
