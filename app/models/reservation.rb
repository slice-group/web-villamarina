#Generado por keppler
require 'elasticsearch/model'
class Reservation < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  has_many :rooms, through: :orders
  has_many :orders
  accepts_nested_attributes_for :orders, :reject_if => :all_blank, :allow_destroy => true
  validates_presence_of :name, :documentation, :email, :telephone, :adults, :checkin, :checkout
  validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create, :message => "Formato inválido" }
  validates :telephone, :format => { :with => /\d{11}/, :on => :create, :message => "Formato inválido" }
  validate :validates_rooms, :validate_fields, :valid_check_in, :valid_date_range_required, :orders_filter

  after_commit on: [:update] do
    puts __elasticsearch__.index_document
  end
  
  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      name:  self.name,
      documentation:  self.documentation,
      email:  self.email,
      telephone:  self.telephone,
      adults:  self.adults.to_s,
      kids:  self.kids.to_s,
      babies:  self.babies.to_s,
      origin:  self.origin,
      motive:  self.motive,
      checkin:  self.checkin,
      checkout:  self.checkout,
      payment:  self.payment.to_s,
      observations:  self.observations,
    }.as_json
  end

  def reservation_days
    (self.checkout.to_date - self.checkin.to_date).to_i
  end

  def total_price_for_night
    self.orders.inject(0){ |total, item | total += item.quantity.to_i * item.room.price.to_i }
  end

  def price_total
    self.total_price_for_night*reservation_days
  end

  def orders_filter
    self.orders.each do |order|
      if order.quantity.blank?
        self.orders.delete(order)
      end     
    end
  end

  def valid_date_range_required
    unless self.checkin.blank? or self.checkout.blank?
      errors.add(:checkout, "El check-out no puede ser despues del check-in") if self.checkin > self.checkout
    end
  end

  def valid_check_in
    unless self.checkin.blank? or self.checkout.blank?
      errors.add(:checkin, "El check-in que usted ingresó no es valido") if self.checkin < Date.parse(Time.new.to_s)
      errors.add(:checkout, "El check-in que usted ingresó no es valido") if self.checkout < Date.parse(Time.new.to_s)
    end
  end

  def validates_rooms
    room_hash = []
    self.orders.each do |order|
      unless order.quantity.blank?
        room_hash << order.quantity
      end
    end
    if room_hash.blank?
      errors.add(:orders, "Debe elegir una habitación")
    end
  end

  def validate_fields
    errors.add(:payment, "Debe elegir una forma de pago") if self.payment == 0
    errors.add(:errors, "La cantidad de Niños es inválida") if self.kids.to_i < 0
    errors.add(:errors, "La cantidad de Bebes es inválida") if self.babies.to_i < 0
    errors.add(:errors, "La cantidad de Adultos es inválida") if self.adults.to_i < 1
  end

end
#Reservation.import
