$(document).ready(function(){
	$('.wilmored-carousel-big').slick({
		infinite: true,
		fade: true,
		autoplay: true,
		autoplaySpeed: 2000,
		arrows: false,
	});
});