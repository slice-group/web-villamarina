function removeHash () { 
  history.pushState("", document.title, window.location.pathname + window.location.search);
}

function scrollToAnchor(anchor){
  $('#'+anchor).animatescroll({
    scrollSpeed:1000
  }
)}

$( document ).ready(function() {

  var sections = $('section'), nav = $('.navbar-nav'), nav_height = nav.outerHeight();
 
  $(window).on('scroll', function () {
    var cur_pos = $(this).scrollTop();
   
    sections.each(function() {
      var top = $(this).offset().top - nav_height,
          bottom = top + $(this).outerHeight();
   
      if (cur_pos >= top && cur_pos <= bottom) {
        nav.find('li').removeClass('current');
        sections.removeClass('current');
   
        $(this).addClass('current');
        nav.find('#link-'+$(this).attr('id')).addClass('current');
      }
    });
  });


  document.onscroll = function() {
    if( $(window).scrollTop() > $('#home').height() - 80 ) {
      $('.navbar').removeClass('navbar-static').addClass('navbar-fixed-top');
    }
    else {
      $('.navbar').removeClass('navbar-fixed-top').addClass('navbar-static');
    }
  };
  
});