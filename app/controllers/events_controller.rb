class EventsController < ApplicationController
  layout 'layouts/frontend/application'

  # GET /eventos/new
  def new
    @event = Event.new
  end

  # POST /eventos
  # POST /eventos.json
  def create
    params[:event].parse_time_select!(:begin_time)
    params[:event].parse_time_select!(:end_time)
    if !event_params[:begin_time].to_s.empty? && !event_params[:end_time].to_s.empty?
      params[:event][:begin_time] = params[:event][:begin_time].strftime("%I:%M %p")
      params[:event][:end_time] = params[:event][:end_time].strftime("%I:%M %p")
    end
    @event = Event.new(event_params)

    respond_to do |format|
      if @event.valid?
        EventMailer.event_email(@event).deliver_now
        format.html { redirect_to root_path, notice: @event.name+', La solicitud de reserva para su evento ya ha sido enviada' }
        format.json { render action: 'show', status: :created, location: @event }
      else
        format.html { render action: 'new' }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:name, :ci, :company, :date, :email, :telephone, :event_type, :quantity, :begin_date, :end_date, :begin_time, :end_time, :assembly_type, :event_description, :cold_snacks, :hot_snacks, :dish_menu, :buffet_style, :theme_stations, :morning_coffeebreak, :evening_coffeebreak, :cheese_table, :uncorking_coctels, :uncorking_whisky, :uncorking_wine, :uncorking_vodka, :coctel_bar, :sodas, :color_theme, :flower_arrengements, :baloons, :toldo, :sound, :dj, :musical_group, :podium, :videobeam, :lights, :inflatable, :magic_show, :dance_show, :crazy_hour, :other_services, :observations)
    end
end
