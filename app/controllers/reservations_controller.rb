#Generado con Keppler.
class ReservationsController < ApplicationController  
  before_filter :authenticate_user!, except: [:new, :create]
  layout :resolve_layout
  load_and_authorize_resource except: [:new, :create]
  before_action :set_reservation, only: [:show, :edit, :update, :destroy]

  # GET /reservations
  def index
    reservations = Reservation.searching(@query).all
    @objects, @total = reservations.page(@current_page), reservations.size
    redirect_to reservations_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /reservations/1
  def show
  end

  # GET /reservations/new
  def new
    @reservation = Reservation.new(checkin: params[:checkin], checkout: params[:checkout])
  end

  # GET /reservations/1/edit
  def edit
  end

  # POST /reservations
  def create
    puts params
    @reservation = Reservation.new(reservation_params)
    if @reservation.save
      redirect_to root_path, notice: reservation_params[:name]+', Su solicitud de reserva debe ser confirmada.'
      ReservationMailer.reservation_email(reservation_params, "http://localhost:3000/admin/reservations/#{Reservation.last.id}").deliver_now
    else
      render :new
    end
  end

  # PATCH/PUT /reservations/1
  def update
    if @reservation.update(reservation_params)
      redirect_to @reservation, notice: 'Reservation was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /reservations/1
  def destroy
    @reservation.destroy
    redirect_to reservations_url, notice: 'Reservation was successfully destroyed.'
  end

  def destroy_multiple
    Reservation.destroy redefine_ids(params[:multiple_ids])
    redirect_to reservations_path(page: @current_page, search: @query), notice: "Reservaciones eliminados satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation
      @reservation = Reservation.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def reservation_params
      params.require(:reservation).permit(:name, :documentation, :email, :telephone, :adults, :kids, :babies, :origin, :motive, :checkin, :checkout, :payment, :observations, orders_attributes: [:id, :room_id, :quantity, :_destroy])
    end

    def resolve_layout
      case action_name
      when "destroy", "update", "edit", "show", "index"
        'admin/application'
      else
        'frontend/application'
      end
    end

end
